+++
title = "Birstall Bike Brigade?"
description = "Let's go ride our bikes and stuff"
+++

TL;CBATR (Too Long; Can't Be Arsed To Read)  
I'm Craven, I ride bikes, my bike is pictured below if you regonise it. I want to meet up at the Birstall bike track at a set time and date so we can build a group of riders who regularly muck about, and also to imrpove the track and trails around it. Hit me up via the email cam12win [at] gmail [dot] com, or on [Telegram at MxCraven](https://t.me/mxcraven). 


Hi, I'm Craven. I enjoy riding bikes, and have spent a lot of time riding bikes, and especially riding around the Birstall track. I've been trying for a little while to find people to more regularly call up and ride with, but it's been hard finding people by pure luck of rocking up at the same time. So what's better than trying to be here 24/7? Having a bit of paper do it for me. I assume you saw the flyer. This site is because I ramble and jamble for ages, and sometimes things need clarifying and editing. 

Here's a picture of my bike: 
![](IMG_20220303_165723.jpg "A black mountain bike with orange pedals and grips. It has gold/yellow accents too.")

If you regognise that you've seen me at the track I guess (also old tyre for referance, the colours used to be the other way around). 

So what do I really want to do, after the ramble is over, well I should have put that on the poster :DD I want to set up a club/riding group that meets at a regular time (17:00ish on Friday evenings? Go until we get tired?) at the bike track in Birstall. I go on road rides, and having a regular time has really increased the number of people that joined a local group, but jumps are much more fun than cafe miles. 

If you're interested, there's a few ground rules that need to be around so everyone plays fair...

1. Run what you brung. No need to be ashamed about what bike you ride, or how you ride it. We're all riding the wrong kit really, and everyone had a shitbox at some point. 
2. No bigotry, homophobia, transphobia, racism, or any of that shit. We're here to ride bikes and enjoy it. 
3. Anyone welcome, no matter the age (unless you break the other rules, then get out). There's probably a vague maturity limit, but it's a public park so we'll need to put up with small kids anyway. 
4. Don't leave trash, we've gotta respect the place else the council will find reason to shut it down. 

So uhh, yeah. See the above TLDR for contact info, I do have a few others but email and Telegram are my mains, but feel free to email me and see what I've got. I do *not* have Facebook, or any other services owned by them (Whatsapp, Instagram, Messenger and whathaveyou). 

So uhh, yeah, if you somehow read this whole ramble check out my [rambly blog over here](http://bike.magnoren.uk/)

Looking forwards to seeing you at the track! 